<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Cities;

// Interfaces for dependencies
use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Hotel as HotelRegistry;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels as HotelCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\City as CityInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as EntityInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Data as DataInterface;

use AdaLiszk\Trivago\Recruiting\Providers\Entity;

/**
 * Class HotelEntity
 *
 * Just a Hotel type Entity
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package AdaLiszk\Trivago\Recruiting
 * @subpackage Hotels
 * @category Entity
 * @since v1.0.0
 */
class City extends Entity implements CityInterface
{
    /** @var array of required properties for fill with data */
    protected $fillable = ['id','name','hotels'];

    /** @var string */
    protected $name;

    /** @var string */
    protected $address;

    /** @var HotelCollection */
    protected $hotels;

    /** @var HotelRegistry */
    protected $hotelRegistry;

    /**
     * Get dependencies and initialize
     *
     * @param DataInterface|null $dataSet NOTE: I cannot set the type in arguments because the dependency resolver will try to fill the gap.
     * @param HotelRegistry $hotelRegistry
     */
    public function __construct($dataSet = null, HotelRegistry $hotelRegistry)
    {
        $this->hotelRegistry = $hotelRegistry;

        if (!empty($dataSet))
        {
            $this->initializeProperties($dataSet);

            // NOTE: I don't need to check it's existence because initializeProperties throws Exception in that case
            $this->initializeHotels($dataSet['hotels']);
        }

    }

    /**
     * Initialize HotelCollection
     * @param iterable $hotels
     */
    private function initializeHotels(iterable $hotels)
    {
        if (!($hotels instanceof HotelCollection))
        {

            $this->hotels = null;
            foreach ($hotels as $id)
            {
                if (empty($this->hotels)) $this->hotels = $this->hotelRegistry->getById($id);
                else $this->hotels->unionWith($this->hotelRegistry->getById($id));
            }
        }
    }

    /**
     * Getter for Name property
     * @return string
     */
    public function name(): string { return $this->name; }

    /**
     *
     * Getter for Hotels property
     *
     * @return HotelCollection
     */
    public function hotels(): HotelCollection
    {
        return $this->hotels;
    }

    /**
     * Creating a new instance from the same Entity
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @param DataInterface|null $initialData
     * @return CityInterface|EntityInterface
     */
    public function newInstance(?DataInterface $initialData = null): EntityInterface
    {
        return new self($initialData, $this->hotelRegistry);
    }
}