<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\DataSources;

use AdaLiszk\Trivago\Recruiting\Boundaries\FileDataSource;
use SplFileObject as FileObject;

class JSONData implements FileDataSource
{
    /** @var FileObject */
    private $file;

    /**
     * Open a connection to the sourceName which can be a filename or a collection name
     *
     * @param string $sourceFilename
     * @return bool
     */
    public function open(string $sourceFilename): bool
    {
        $this->file = new FileObject($sourceFilename);

        return $this->file->isReadable();
    }

    /**
     * Open a connection to the sourceName which can be a filename or a collection name
     *
     * @return iterable
     */
    public function getContent(): iterable
    {
        // @TODO: Write some stuff which could read and parse JSON while it's reading the file
        // For now I wont make it better, since it's just a demo
        $contents = ""; while (!$this->file->eof()) $contents .= $this->file->fgets();
        $parsed = json_decode($contents);

        return !empty($parsed) ? $parsed : [];
    }
}