<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Partners;

use AdaLiszk\Trivago\Recruiting\Boundaries\FileDataSource as DataSourceInterface;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;

use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Partner as PartnerRegistryInterface;
use AdaLiszk\Trivago\Recruiting\Providers\Registry as RegistryProvider;

/**
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 */
class Registry extends RegistryProvider implements PartnerRegistryInterface
{
    /**
     * Get the Dependencies
     *
     * @param DataSourceInterface $dataSource
     * @internal param DataSourceInterface $database
     */
    public function __construct(DataSourceInterface $dataSource)
    {
        parent::__construct($dataSource, DATA_PATH . 'partners.json');
    }

    /**
     * Basic query for an Entity
     *
     * @param int $id
     * @return CityEntityInterface|CollectionInterface
     */
    public function getById(int $id): CollectionInterface
    {
        // TODO: Implement getById() method.
    }

    /**
     * Get all Results
     *
     * @return CollectionInterface
     */
    public function getAll(): CollectionInterface
    {
        // TODO: Implement get() method.
    }
}