<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Partners;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Partners as PartnerCollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\Partner as PartnerEntityInterface;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as EntityInterface;

use AdaLiszk\Trivago\Recruiting\Providers\Collection as CollectionProvider;
use InvalidArgumentException;

/**
 * Class Hotels\Collection
 *
 * It checks the Entity type so it would be the allowed PartnerEntity
 * Sadly PHP doesn't support interface inheritance properly yet.
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package AdaLiszk\Trivago\Recruiting
 * @subpackage Partners
 * @category Collection
 * @since v1.0.0
 */
class Collection extends CollectionProvider implements PartnerCollectionInterface
{
    /**
     * Add item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PartnerEntityInterface|EntityInterface $item
     * @param mixed $data
     */
    public function add(EntityInterface $item, $data = NULL): void
    {
        if (!($item instanceof PartnerEntityInterface))
            throw new InvalidArgumentException('Item is not instance of ' . PartnerEntityInterface::class);

        $this->attach($item, $data);
    }

    /**
     * Remove item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PartnerEntityInterface|EntityInterface $item
     */
    public function remove(EntityInterface $item): void
    {
        if (!($item instanceof PartnerEntityInterface))
            throw new InvalidArgumentException('Item is not instance of ' . PartnerEntityInterface::class);

        $this->detach($item);
    }

    /**
     * Make Union with a given collection and the result would be the base Collection having all of the items
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PartnerCollectionInterface|CollectionInterface $collection
     */
    public function unionWith(CollectionInterface $collection): void
    {
        if (!($collection instanceof PartnerCollectionInterface))
            throw new InvalidArgumentException('Item is not instance of ' . PartnerCollectionInterface::class);

        /** @var \SplObjectStorage $collection (it's the base class, but somehow my IDE complains about it) */
        $this->addAll($collection);
    }

    /**
     * Subtract items from the collections using an other collection
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PartnerCollectionInterface|CollectionInterface $collection
     */
    public function subtractItems(CollectionInterface $collection): void
    {
        if (!($collection instanceof PartnerCollectionInterface))
            throw new InvalidArgumentException('Item is not instance of ' . PartnerCollectionInterface::class);

        /** @var \SplObjectStorage $collection (it's the base class, but somehow my IDE complains about it) */
        $this->removeAll($collection);
    }

    /**
     * Subtract items from the collection but keeping the given collection items if they are in the base collection
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PartnerCollectionInterface|CollectionInterface $collection
     */
    public function intersectWith(CollectionInterface $collection): void
    {
        if (!($collection instanceof PartnerCollectionInterface))
            throw new InvalidArgumentException('Item is not instance of ' . PartnerCollectionInterface::class);

        /** @var \SplObjectStorage $collection (it's the base class, but somehow my IDE complains about it) */
        $this->removeAllExcept($collection);
    }
}
