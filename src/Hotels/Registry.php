<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Hotels;


use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Cities as CityCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\FileDataSource as DataSourceInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels as HotelCollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\Hotel as HotelEntityInterface;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Hotel as HotelRegistryInterface;
use AdaLiszk\Trivago\Recruiting\Providers\Registry as RegistryProvider;

use ReflectionClass;

/**
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 */
class Registry extends RegistryProvider implements HotelRegistryInterface
{
    private $hotelClass;
    private $hotelCollectionClass;

    protected $entities = [];
    protected $names = [];

    /**
     * Get the Dependencies
     *
     * @param DataSourceInterface $dataSource
     * @param HotelEntityInterface $hotelEntity
     * @param HotelCollectionInterface $hotelCollection
     * @internal param DataSourceInterface $database
     */
    public function __construct(
        DataSourceInterface $dataSource,
        HotelEntityInterface $hotelEntity,
        HotelCollectionInterface $hotelCollection
    ) {
        parent::__construct($dataSource, DATA_PATH . 'hotels.json');

        $this->hotelCollectionClass = new ReflectionClass($hotelCollection);
        $this->hotelClass = new ReflectionClass($hotelEntity);

        foreach ($this->db->getContent() as $item)
        {
            /** @var HotelEntityInterface $hotel */
            $hotel = $hotelEntity->newInstance($item);

            // Getting property pointers into memory for less cpu work
            $id = $hotel->id(); $name = $hotel->name();

            $this->entities[$id] = $hotel;
            $this->names[$name] =& $this->entities[$id];
        }
    }

    /**
     * Basic query for an Entity
     *
     * @param int $id
     * @return HotelCollectionInterface|CollectionInterface
     */
    public function getById(int $id): CollectionInterface
    {
        /** @var HotelCollectionInterface $collection */
        $collection = $this->hotelCollectionClass->newInstance();

        if (isset($this->entities[$id]))
            $collection->add($this->entities[$id]);

        return $collection;
    }

    /**
     * Returning cities with the expected name
     *
     * @param string $cityName
     * @return HotelCollectionInterface
     */
    public function getByName(string $cityName): HotelCollectionInterface
    {
        /** @var HotelCollectionInterface $collection */
        $collection = $this->hotelCollectionClass->newInstance();

        if (isset($this->names[$cityName]))
            $collection->add($this->names[$cityName]);

        return $collection;
    }

    /**
     * Getting by cities
     *
     * @param CityCollection $cities
     * @return HotelCollectionInterface
     */
    public function getByCities(CityCollection $cities): HotelCollectionInterface
    {
        // TODO: Implement getByCities() method.
    }

    /**
     * Get all Results
     *
     * @return HotelCollectionInterface|CollectionInterface
     */
    public function getAll(): CollectionInterface
    {
        /** @var HotelCollectionInterface $collection */
        $collection = $this->hotelCollectionClass->newInstance();

        foreach ($this->entities as $cityEntity)
            $collection->add($cityEntity);

        return $collection;
    }
}