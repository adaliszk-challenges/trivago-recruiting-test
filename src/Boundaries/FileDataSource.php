<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Boundaries;

interface FileDataSource
{
    /**
     * Open a connection to the sourceName which can be a filename or a collection name
     *
     * @param string $sourceFilename
     * @return bool
     */
    public function open(string $sourceFilename): bool;

    /**
     * Get the file content from the given format
     *
     * @return iterable
     */
    public function getContent(): iterable;
}
