<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Boundaries\Services;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels as HotelCollection;
use InvalidArgumentException;

/**
 * The main feature is resolving the id of the city from the given city name
 * The secondary feature is to sort the returning result from the partner service in whatever way.
 */
interface Hotels
{
    /**
     * @param string $cityName Name of the city to search for.
     * @throws InvalidArgumentException if city name is unknown.
     * @return HotelCollection
     */
    public function getByCityName(string $cityName): HotelCollection;
}