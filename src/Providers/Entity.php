<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Providers;

use AdaLiszk\Trivago\Recruiting\Boundaries\Data as DataInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as EntityInterface;
use InvalidArgumentException;
use DomainException;

abstract class Entity implements EntityInterface
{
    /** @var array of required properties for fill with data */
    protected $fillable = ['id'];

    /** @var int */
    protected $id;

    /**
     * Initialize properties
     * @param DataInterface $dataSet
     */
    public function initializeProperties($dataSet)
    {
        if (is_object($dataSet)) $dataSet = (array) $dataSet;

        if (!($dataSet instanceof DataInterface))
            throw new DomainException("Wrong data format, it should be instance of " . DataInterface::class);

        // Quickly load the data values into the class properties
        foreach ($dataSet as $key => $value)
            if (!in_array($key, $this->fillable)) throw new InvalidArgumentException("Undefined property name: {$key}");
            else
            {
                $idx = array_search($key, $this->fillable);
                unset($this->fillable[$idx]);
                $this->$key = $value;
            }

        if (count($this->fillable) > 0)
            throw new DomainException("Missing data during initialization: ".implode(',', $this->fillable));
    }

    /**
     * Creating a new instance from the same Entity
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @param DataInterface|null $initialData
     * @return EntityInterface
     */
    public abstract function newInstance(?DataInterface $initialData = null): EntityInterface;

    /**
     * Getter for the ID property
     *
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * Serialize object
     */
    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    /**
     * Recover for serialized state
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }
}