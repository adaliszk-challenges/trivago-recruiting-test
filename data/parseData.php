<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Just parse the initial data quickly for the application registries
// since the current format is not efficient to handle any data

chdir(__DIR__);

$dataContent = file_get_contents('15475.json');
$dataSet = json_decode($dataContent, true);

$cities = [];
$hotels = [];
$partners = [];
$prices = [];

$city = $dataSet;
//foreach($dataSet as $city)
{
    $cityHotels = [];
    foreach ($city['hotels'] as $hotelId => $hotel)
    {
        $hotelPartners = [];
        foreach ($hotel['partners'] as $partnerId => $partner)
        {
            $partnerPrices = [];
            foreach ($partner['prices'] as $priceId => $price)
            {
                $partnerPrices[] = $priceId;
                $price['id'] = $priceId;
                $price['partnerId'] = $partnerId;
                $prices[$priceId] = $price;
            }

            $hotelPartners[] = $partnerId;
            $partner['id'] = $partnerId;
            $partner['hotelId'] = $hotelId;
            $partner['prices'] = $partnerPrices;
            $partners[$partnerId] = $partner;
        }

        $cityHotels[] = $hotelId;
        $hotel['id'] = $hotelId;
        $hotel['cityId'] = $city['id'];
        $hotel['partners'] = $hotelPartners;
        $hotels[$hotelId] = $hotel;
    }

    $city['name'] = $city['city'];
    unset($city['city']);

    $city['hotels'] = $cityHotels;
    $cities[] = $city;
}

file_put_contents('cities.json', json_encode($cities));
file_put_contents('hotels.json', json_encode($hotels));
file_put_contents('partners.json', json_encode($partners));
file_put_contents('prices.json', json_encode($prices));