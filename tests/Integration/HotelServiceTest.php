<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Tests\Integration;

use AdaLiszk\Trivago\Recruiting\Tests\TestCase;

class HotelServiceTest extends TestCase
{
    /**
     * Sanity check: with the correct data it has any output at all?
     *
     * @test
     */
    public function itHasOutput()
    {
        /** @var \AdaLiszk\Trivago\Recruiting\Boundaries\Services\Hotels $hotelService */
        $hotelService = require ROOT_PATH . 'bootstrap/hotelService.php';

        /** @var \AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels $hotels */
        $hotels = $hotelService->getByCityName('Düsseldorf');

        $this->assertNotEmpty($hotels);
    }

    /**
     * Check for the not mapped data, it should throw an InvalidArgumentException
     *
     * @depends isHotelServiceHasOutput
     * @test
     */
    public function itThrowsException()
    {
        /** @var \AdaLiszk\Trivago\Recruiting\Boundaries\Services\Hotels $hotelService */
        $hotelService = require ROOT_PATH . 'bootstrap/hotelService.php';

        /** @var \AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels $hotels */
        $hotels = $hotelService->getByCityName('Budapest');

        $this->expectException('InvalidArgumentException');
        $this->assertEmpty($hotels);
    }
}