<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Tests;

use PHPUnit\Framework\TestCase as TestCasePresenter;
use PHPUnit_Framework_MockObject_MockObject as MockObjectInterface;
use stdClass;

class TestCase extends TestCasePresenter
{
    protected $composer;
    protected $namespaceRoot;

    protected $container;

    public function setUp()
    {
        parent::setUp();

        require_once __DIR__ . '/../bootstrap/environment.php';

        if (!defined('VERBOSE_MODE')) define('VERBOSE_MODE', true);

        $this->composer = json_decode(file_get_contents(ROOT_PATH . 'composer.json'));
        $this->namespaceRoot = str_replace('\\Tests', '\\', __NAMESPACE__);

        $this->container = require ROOT_PATH . 'bootstrap/container.php';
    }

    /**
     * Setup methods required to mock an iterator
     *
     * @param MockObjectInterface $iteratorMock The mock to attach the iterator methods to
     * @param array $items The mock data we're going to use with the iterator
     * @return MockObjectInterface The iterator mock
     */
    public function createIterableMock(MockObjectInterface $iteratorMock, array $items)
    {
        $position = 0;

        $iteratorMock->expects($this->any())
            ->method('rewind')
            ->will(
                $this->returnCallback(
                    function() use (&$position) {
                        $position = 0;
                    }
                )
            );

        $iteratorMock->expects($this->any())
            ->method('current')
            ->will(
                $this->returnCallback(
                    function() use ($items, $position) {
                        return $items[$position];
                    }
                )
            );

        $iteratorMock->expects($this->any())
            ->method('key')
            ->will(
                $this->returnCallback(
                    function() use ($position) {
                        return $position;
                    }
                )
            );

        $iteratorMock->expects($this->any())
            ->method('next')
            ->will(
                $this->returnCallback(
                    function() use (&$position) {
                        $position++;
                    }
                )
            );

        $iteratorMock->expects($this->any())
            ->method('valid')
            ->will(
                $this->returnCallback(
                    function() use ($items, $position) {
                        return isset($items[$position]);
                    }
                )
            );

        $iteratorMock->expects($this->any())
            ->method('count')
            ->will(
                $this->returnCallback(
                    function() use ($items) {
                        return sizeof($items);
                    }
                )
            );

        return $iteratorMock;
    }

    /**
     * Setup methods required to mock a collection
     *
     * @param MockObjectInterface $collectionMock for mocking collections
     * @param array $items Initial items
     *
     * @return MockObjectInterface The iterator mock
     */
    public function createCollectionMock(MockObjectInterface $collectionMock, array $items = [])
    {
        $position = 0;

        $collectionMock->expects($this->any())
            ->method('add')
            ->will(
                $this->returnCallback(
                    function($item) use (&$items) {
                        $items[] = $item;
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('unionWith')
            ->will(
                $this->returnCallback(
                    function($item) use (&$items) {
                        $items[] = $item;
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('rewind')
            ->will(
                $this->returnCallback(
                    function() use (&$position) {
                        $position = 0;
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('current')
            ->will(
                $this->returnCallback(
                    function() use ($items, $position) {
                        return $items[$position];
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('key')
            ->will(
                $this->returnCallback(
                    function() use ($position) {
                        return $position;
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('next')
            ->will(
                $this->returnCallback(
                    function() use (&$position) {
                        $position++;
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('valid')
            ->will(
                $this->returnCallback(
                    function() use ($items, $position) {
                        return isset($items[$position]);
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('count')
            ->will(
                $this->returnCallback(
                    function() use ($items) {
                        return sizeof($items);
                    }
                )
            );

        return $collectionMock;
    }
}

