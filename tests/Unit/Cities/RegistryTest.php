<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Tests\Unit\Cities;

use AdaLiszk\Trivago\Recruiting\Tests\TestCase;
use ReflectionClass;
use stdClass;

// Interfaces for dependencies
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels as HotelCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\City as CityRegistry;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Cities as CityCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\City as CityEntity;
use AdaLiszk\Trivago\Recruiting\Boundaries\FileDataSource;

/**
 * Tests for Cities\Registry class
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package AdaLiszk\Trivago\Recruiting
 * @subpackage CityRegistry
 * @category Tests
 * @since v1.0.0
 */
class RegistryTest extends TestCase
{
    /** @var ReflectionClass of the class */
    private $class;

    /** @var ReflectionClass of the interface */
    private $interface;

    /**
     * Initialize ReflectionClasses
     */
    public function setUp()
    {
        parent::setUp();

        $this->class = new ReflectionClass($this->namespaceRoot.'Cities\\Registry');
        $this->interface = new ReflectionClass($this->namespaceRoot.'Boundaries\\Registries\\City');
    }

    // ----------------------------------------------------------------------------------------------------------------

    /**
     * Sanity Check: Is the class even implements the desired interface?
     * @test
     */
    public function isImplementsInterface()
    {
        $this->assertArrayHasKey(
            $this->interface->getName(),
            $this->class->getInterfaces(),
            "Class {$this->class->getName()} is not implements {$this->interface->getName()}"
        );
    }

    // ----------------------------------------------------------------------------------------------------------------

    public function initializationDataProvider()
    {
        $datas = json_decode('[{"id":15475,"name":"D\u00fcsseldorf","hotels":[1,2]}]');

        foreach ($datas as $data)
        {
            yield [$data->id, $data->name, $data->hotels, $data];
        }
    }

    /**
     * Check that the register can initialize with the sample data's
     *
     * @param int $entityId
     * @param string $entityName
     * @param iterable $entityItems
     * @param stdClass $data
     *
     * @return CityRegistry
     * @dataProvider initializationDataProvider
     * @dependsisImplementsInterface
     * @test
     */
    public function canInitialize(int $entityId, string $entityName, iterable $entityItems, stdClass $data)
    {
        $dataSource = $this->createMock(FileDataSource::class);
        $dataSource->expects($this->once())->method('open')->willReturn(true);
        $dataSource->expects($this->any())->method('getContent')->willReturn([$data]);

        $hotelCollection = $this->createMock(HotelCollection::class);
        $hotelCollection->method('unionWith');

        $cityEntity = $this->createMock(CityEntity::class);
        $cityEntity->expects($this->once())->method('id')->willReturn($entityId);
        $cityEntity->expects($this->once())->method('name')->willReturn($entityName);
        $cityEntity->method('hotels')->willReturn($entityItems);
        $cityEntity->expects($this->once())->method('newInstance')->willReturnSelf();

        $cityCollection = $this->createMock(CityCollection::class);

        $className = $this->class->getName();
        return new $className($dataSource, $cityEntity, $cityCollection);
    }

    /**
     * Check that the register data based on Id
     *
     * @param int $entityId
     * @param string $entityName
     * @param iterable $entityItems
     * @param stdClass $data
     *
     * @dataProvider initializationDataProvider
     * @depends canInitialize
     * @test
     */
    public function canGetAll(int $entityId, string $entityName, iterable $entityItems, stdClass $data)
    {
        $registry = $this->canInitialize($entityId, $entityName, $entityItems, $data);
        $result = $registry->getAll();

        // Just checking that the instance is a CityCollection, we don't test the result since we not mocking
        // the collection properly, that's an other TestCase job to test.
        $this->assertInstanceOf(CityCollection::class, $result);
    }

    /**
     * Check that the register data based on Id
     *
     * @param int $entityId
     * @param string $entityName
     * @param iterable $entityItems
     * @param stdClass $data
     *
     * @dataProvider initializationDataProvider
     * @depends canInitialize
     * @test
     */
    public function canGetById(int $entityId, string $entityName, iterable $entityItems, stdClass $data)
    {
        $registry = $this->canInitialize($entityId, $entityName, $entityItems, $data);
        $result = $registry->getById($entityId);

        // Just checking that the instance is a CityCollection, we don't test the result since we not mocking
        // the collection properly, that's an other TestCase job to test.
        $this->assertInstanceOf(CityCollection::class, $result);
    }

    /**
     * Check that the register data based on Id
     *
     * @param int $entityId
     * @param string $entityName
     * @param iterable $entityItems
     * @param stdClass $data
     *
     * @dataProvider initializationDataProvider
     * @depends canInitialize
     * @test
     */
    public function canGetByName(int $entityId, string $entityName, iterable $entityItems, stdClass $data)
    {
        $registry = $this->canInitialize($entityId, $entityName, $entityItems, $data);
        $result = $registry->getByName($entityName);

        // Just checking that the instance is a CityCollection, we don't test the result since we not mocking
        // the collection properly, that's an other TestCase job to test.
        $this->assertInstanceOf(CityCollection::class, $result);
    }
}
