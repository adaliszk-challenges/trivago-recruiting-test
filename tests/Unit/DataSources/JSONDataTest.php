<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Tests\Unit\DataSources;

use AdaLiszk\Trivago\Recruiting\DataSources\JSONData;
use AdaLiszk\Trivago\Recruiting\Tests\TestCase;
use org\bovigo\vfs\vfsStream as FileSystem;
use ReflectionClass;

class JSONDataTest extends TestCase
{
    /** @var ReflectionClass of the class */
    private $class;

    /** @var ReflectionClass of the interface */
    private $interface;

    /**
     * Initialize ReflectionClasses
     */
    public function setUp()
    {
        parent::setUp();

        $this->class = new ReflectionClass($this->namespaceRoot.'DataSources\\JSONData');
        $this->interface = new ReflectionClass($this->namespaceRoot.'Boundaries\\FileDataSource');
    }

    // ----------------------------------------------------------------------------------------------------------------

    /**
     * Sanity Check: Is the class even implements the desired interface?
     * @test
     */
    public function isImplementsInterface()
    {
        $this->assertArrayHasKey(
            $this->interface->getName(),
            $this->class->getInterfaces(),
            "Class {$this->class->getName()} is not implements {$this->interface->getName()}"
        );
    }

    // ----------------------------------------------------------------------------------------------------------------

    public function fileProviders()
    {
        $files = [
            'hello_world.json' => [["hello" => "world"]],
            'cities.json' => [["id" => 15475, "name" => "Düsseldorf", "hotels" => [ 1, 2 ]]],
        ];

        FileSystem::setup('data');

        foreach($files as $fileName => $fileContent)
        {
            $file = FileSystem::url("data/{$fileName}");
            file_put_contents($file, json_encode($fileContent));

            yield [$file, $fileContent];
        }
    }

    /**
     *
     * @param string $fileName
     * @param iterable $fileContent
     *
     * @dataProvider fileProviders
     * @test
     */
    public function canOpenFileName(string $fileName, iterable $fileContent)
    {
        $className = $this->class->getName();
        /** @var JSONData $instance */
        $instance = new $className();

        $isReadable = $instance->open($fileName);

        $this->assertTrue($isReadable);
    }

    /**
     *
     * @param string $fileName
     * @param iterable $fileContent
     *
     * @dataProvider fileProviders
     * @test
     */
    public function canReadFileContent(string $fileName, iterable $fileContent)
    {
        $className = $this->class->getName();
        /** @var JSONData $instance */
        $instance = new $className();
        $instance->open($fileName);

        foreach ($instance->getContent() as $idx => $item)
        {
            $this->assertSame(serialize((object) $fileContent[$idx]), serialize($item));
        }
    }

    // ----------------------------------------------------------------------------------------------------------------
}