<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Tests\Unit\Hotels;

use AdaLiszk\Trivago\Recruiting\Tests\TestCase;
use PHPUnit_Framework_MockObject_MockObject as MockedObject;
use ReflectionClass;

// Interfaces for dependencies
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels as HotelCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\Hotel as HotelEntity;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as Collection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as Entity;

/**
 * Tests for Hotels\Collection class
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package AdaLiszk\Trivago\Recruiting
 * @subpackage HotelCollection
 * @category Tests
 * @since v1.0.0
 */
class CollectionTest extends TestCase
{
    /** @var ReflectionClass of the class */
    private $class;

    /** @var ReflectionClass of the interface */
    private $interface;

    /** @var ReflectionClass of the interface */
    private $item;

    /**
     * Initialize ReflectionClasses
     */
    public function setUp()
    {
        parent::setUp();

        $this->class = new ReflectionClass($this->namespaceRoot.'Hotels\\Collection');
        $this->interface = new ReflectionClass($this->namespaceRoot.'Boundaries\\Collections\\Hotels');
        $this->item = new ReflectionClass($this->namespaceRoot.'Boundaries\\Entities\\Hotel');
    }

    /**
     * Sanity Check: Is the class even implements the desired interface?
     * @test
     */
    public function isImplementsInterface()
    {
        $this->assertArrayHasKey(
            $this->interface->getName(),
            $this->class->getInterfaces(),
            "Class {$this->class->getName()} is not implements {$this->interface->getName()}"
        );
    }

    // ----------------------------------------------------------------------------------------------------------------

    public function itemProvider()
    {
        $item1 = $this->createMock(HotelEntity::class);
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);

        return [
            'I1'    => [$item1],
        ];
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @param MockedObject $item
     * @return HotelCollection
     *
     * @dataProvider itemProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canReceiveItem(MockedObject $item)
    {
        /** @var HotelCollection $instance */
        $instance = $this->class->newInstance();

        /** @var HotelEntity $item */
        $instance->add($item);

        // Sanity check: it has only one item?
        $this->assertCount(1, $instance);

        // Then this will only iterate once
        foreach($instance as $retrievedItem)
            $this->assertSame($item, $retrievedItem);

        return $instance;
    }

    /**
     * Checking that it is only allow the subtype to be added
     *
     * @depends canReceiveItem
     * @test
     */
    public function isCheckingSubtypeForReceiving()
    {
        /** @var HotelCollection $instance */
        $instance = $this->class->newInstance();

        $item = $this->createMock(Entity::class);
        $this->expectException('InvalidArgumentException');

        /** @var Entity $item */
        $instance->add($item);
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @return HotelCollection
     *
     * @depends canReceiveItem
     * @test
     */
    public function canDetectContaining()
    {
        /** @var HotelCollection $instance */
        $instance = $this->class->newInstance();

        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        /** @var HotelEntity $item */
        $instance->add($item);

        $this->assertTrue($instance->containing($item));
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @depends canReceiveItem
     * @test
     */
    public function canRemoveItem()
    {
        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        $instance = $this->canReceiveItem($item);

        /** @var HotelEntity $item */
        $instance->remove($item);

        // There is no other item left?
        $this->assertCount(0, $instance);
    }

    /**
     * Checking that it is only allow the subtype to be added
     *
     * @depends canRemoveItem
     * @test
     */
    public function isCheckingSubtypeForRemoving()
    {
        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        /** @var HotelCollection $instance */
        $instance = $this->canReceiveItem($item);

        $item = $this->createMock(Entity::class);
        $this->expectException('InvalidArgumentException');

        /** @var Entity $item */
        $instance->remove($item);
    }

    // ----------------------------------------------------------------------------------------------------------------

    /**
     * Checking that it is only allow the subtype to merged with
     *
     * @test
     */
    public function isCheckingSubtypeForUnion()
    {
        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        /** @var HotelCollection $instance */
        $hotelCollection = $this->canReceiveItem($item);
        $regularCollection = $this->createMock(Collection::class);

        // It should throw an exception since it's not a subtype
        $this->expectException('InvalidArgumentException');

        /** @var Collection $regularCollection */
        $hotelCollection->unionWith($regularCollection);
    }

    // ----------------------------------------------------------------------------------------------------------------

    /**
     * Checking that it is only allow the subtype to merged with
     *
     * @test
     */
    public function isCheckingSubtypeForSubtraction()
    {
        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        /** @var HotelCollection $instance */
        $hotelCollection = $this->canReceiveItem($item);
        $regularCollection = $this->createMock(Collection::class);

        // It should throw an exception since it's not a subtype
        $this->expectException('InvalidArgumentException');

        /** @var Collection $regularCollection */
        $hotelCollection->subtractItems($regularCollection);
    }

    // ----------------------------------------------------------------------------------------------------------------

    /**
     *
     * @test
     */
    public function isCheckingSubtypeForIntersection()
    {
        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        /** @var HotelCollection $instance */
        $hotelCollection = $this->canReceiveItem($item);
        $regularCollection = $this->createMock(Collection::class);

        // It should throw an exception since it's not a subtype
        $this->expectException('InvalidArgumentException');

        /** @var Collection $regularCollection */
        $hotelCollection->intersectWith($regularCollection);
    }
}