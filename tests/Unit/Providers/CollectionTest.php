<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Tests\Unit\Providers;

use AdaLiszk\Trivago\Recruiting\Tests\TestCase;
use PHPUnit_Framework_MockObject_MockObject as MockedObject;
use ReflectionClass;

// Interfaces for dependencies
use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as EntityInterface;


/**
 * Tests for Providers\Collection class
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package AdaLiszk\Trivago\Recruiting
 * @subpackage CollectionInterface
 * @category Tests
 * @since v1.0.0
 */
class CollectionTest extends TestCase
{
    /** @var ReflectionClass of the class */
    private $class;

    /** @var ReflectionClass of the interface */
    private $interface;

    /** @var ReflectionClass of the interface */
    private $item;

    // ----------------------------------------------------------------------------------------------------------------

    /**
     * Initialize ReflectionClasses
     */
    public function setUp()
    {
        parent::setUp();

        $this->class = new ReflectionClass(__NAMESPACE__.'\\Collection');
        $this->interface = new ReflectionClass($this->namespaceRoot.'Boundaries\\Collection');
        $this->item = new ReflectionClass($this->namespaceRoot.'Boundaries\\Entity');
    }

    // ----------------------------------------------------------------------------------------------------------------

    /**
     * Sanity Check: Is the class even implements the desired interface?
     * @test
     */
    public function isImplementsInterface()
    {
        $this->assertArrayHasKey(
            $this->interface->getName(),
            $this->class->getInterfaces(),
            "Class {$this->class->getName()} is not implements {$this->interface->getName()}"
        );
    }

    // ----------------------------------------------------------------------------------------------------------------

    public function itemProvider()
    {
        $item1 = $this->createMock(EntityInterface::class);
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);

        return [
            'I1'    => [$item1],
        ];
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @param MockedObject $item
     * @return CollectionInterface
     *
     * @dataProvider itemProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canReceiveItem(MockedObject $item)
    {
        /** @var CollectionInterface $instance */
        $instance = $this->class->newInstance();

        /** @var EntityInterface $item */
        $instance->add($item);

        // Sanity check: it has only one item?
        $this->assertCount(1, $instance);

        // Then this will only iterate once
        foreach($instance as $retrievedItem)
            $this->assertSame($item, $retrievedItem);

        return $instance;
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @return CollectionInterface
     *
     * @depends canReceiveItem
     * @test
     */
    public function canDetectContaining()
    {
        /** @var CollectionInterface $instance */
        $instance = $this->class->newInstance();

        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        /** @var EntityInterface $item */
        $instance->add($item);

        $this->assertTrue($instance->containing($item));
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @depends canReceiveItem
     * @test
     */
    public function canRemoveItem()
    {
        $item = $this->createMock($this->item->getName());
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        $instance = $this->canReceiveItem($item);

        /** @var EntityInterface $item */
        $instance->remove($item);

        // There is no other item left?
        $this->assertCount(0, $instance);
    }

    // ----------------------------------------------------------------------------------------------------------------

    public function unionProvider()
    {
        $this->setUp();

        $item1 = $this->createMock($this->item->getName());
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);
        /** @var EntityInterface $item1 */

        $item2 = $this->createMock($this->item->getName());
        $item2->expects($this->atLeastOnce())->method('id')->willReturn(2);
        /** @var EntityInterface $item2 */

        $item3 = $this->createMock($this->item->getName());
        $item3->expects($this->atLeastOnce())->method('id')->willReturn(3);
        /** @var EntityInterface $item3 */

        $className = $this->class->getName();

        /** @var CollectionInterface $collection1 */
        $collection1 = new $className();
        $collection1->add($item1);

        /** @var CollectionInterface $collection2 */
        $collection2 = new $className();
        $collection2->add($item1);
        $collection2->add($item2);

        /** @var CollectionInterface $collection3 */
        $collection3 = new $className();
        $collection3->add($item2);

        /** @var CollectionInterface $collection4 */
        $collection4 = new $className();
        $collection4->add($item2);
        $collection4->add($item3);

        /** @var CollectionInterface $collection5 */
        $collection5 = new $className();
        $collection5->add($item3);

        return [
            '(I1)+(I1+I2)'        => [clone $collection1, clone $collection2, [$item1, $item2]],
            '(I1)+(I2)'           => [clone $collection1, clone $collection3, [$item1, $item2]],
            '(I1)+(I2+I3)'        => [clone $collection1, clone $collection4, [$item1, $item2, $item3]],
            '(I1)+(I3)'           => [clone $collection1, clone $collection5, [$item1, $item3]],

            '(I1+I2)+(I1)'        => [clone $collection2, clone $collection1, [$item1, $item2]],
            '(I1+I2)+(I2)'        => [clone $collection2, clone $collection3, [$item1, $item2]],
            '(I1+I2)+(I2+I3)'     => [clone $collection2, clone $collection4, [$item1, $item2, $item3]],
            '(I1+I2)+(I3)'        => [clone $collection2, clone $collection5, [$item1, $item2, $item3]],

            '(I2)+(I1)'           => [clone $collection3, clone $collection1, [$item1, $item2]],
            '(I2)+(I1+I2)'        => [clone $collection3, clone $collection2, [$item1, $item2]],
            '(I2)+(I2+I3)'        => [clone $collection3, clone $collection4, [$item2, $item3]],
            '(I2)+(I3)'           => [clone $collection3, clone $collection5, [$item2, $item3]],

            '(I2+I3)+(I1)'        => [clone $collection4, clone $collection1, [$item1, $item2, $item3]],
            '(I2+I3)+(I1+I2)'     => [clone $collection4, clone $collection2, [$item1, $item2, $item3]],
            '(I2+I3)+(I2)'        => [clone $collection4, clone $collection3, [$item2, $item3]],
            '(I2+I3)+(I3)'        => [clone $collection4, clone $collection5, [$item2, $item3]],

            '(I3)+(I1)'           => [clone $collection5, clone $collection1, [$item1, $item3]],
            '(I3)+(I1+I2)'        => [clone $collection5, clone $collection2, [$item1, $item2, $item3]],
            '(I3)+(I2)'           => [clone $collection5, clone $collection3, [$item2, $item3]],
            '(I3)+(I2+I3)'        => [clone $collection5, clone $collection4, [$item2, $item3]],
        ];
    }

    /**
     * Checking that it can merge it's own Type
     *
     * @param CollectionInterface $A
     * @param CollectionInterface $B
     * @param array $expectedResult
     *
     * @dataProvider unionProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canDoUnion(CollectionInterface $A, CollectionInterface $B, array $expectedResult = [])
    {
        $A->unionWith($B);

        if (empty($expectedResult)) $this->assertEmpty($expectedResult);
        else
        {
            $expectedResultItems = [];
            $resultItems = [];

            foreach($A as $item) $resultItems[] = 'I' . $item->id();
            foreach($expectedResult as $item) $expectedResultItems[] = 'I' . $item->id();

            sort($resultItems);
            sort($expectedResultItems);

            $this->assertSame(
                implode('+', $expectedResultItems),
                implode('+', $resultItems)
            );
        }
    }

    // ----------------------------------------------------------------------------------------------------------------

    public function subtractProvider()
    {
        $this->setUp();

        $item1 = $this->createMock($this->item->getName());
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);
        /** @var EntityInterface $item1 */

        $item2 = $this->createMock($this->item->getName());
        $item2->expects($this->atLeastOnce())->method('id')->willReturn(2);
        /** @var EntityInterface $item2 */

        $item3 = $this->createMock($this->item->getName());
        $item3->expects($this->atLeastOnce())->method('id')->willReturn(3);
        /** @var EntityInterface $item3 */

        /** @var CollectionInterface $collection1 */
        $collection1 = $this->class->newInstance();
        $collection1->add($item1);

        /** @var CollectionInterface $collection2 */
        $collection2 = $this->class->newInstance();
        $collection2->add($item1);
        $collection2->add($item2);

        /** @var CollectionInterface $collection3 */
        $collection3 = $this->class->newInstance();
        $collection3->add($item2);

        /** @var CollectionInterface $collection4 */
        $collection4 = $this->class->newInstance();
        $collection4->add($item2);
        $collection4->add($item3);

        /** @var CollectionInterface $collection4 */
        $collection5 = $this->class->newInstance();
        $collection5->add($item3);

        return [
            '(I1)/(I1+I2)'        => [clone $collection1, clone $collection2, []],
            '(I1)/(I2)'           => [clone $collection1, clone $collection3, [$item1]],
            '(I1)/(I2+I3)'        => [clone $collection1, clone $collection4, [$item1]],
            '(I1)/(I3)'           => [clone $collection1, clone $collection5, [$item1]],

            '(I1+I2)/(I1)'        => [clone $collection2, clone $collection1, [$item2]],
            '(I1+I2)/(I2)'        => [clone $collection2, clone $collection3, [$item1]],
            '(I1+I2)/(I2+I3)'     => [clone $collection2, clone $collection4, [$item1]],
            '(I1+I2)/(I3)'        => [clone $collection2, clone $collection5, [$item1, $item2]],

            '(I2)/(I1)'           => [clone $collection3, clone $collection1, [$item2]],
            '(I2)/(I1+I2)'        => [clone $collection3, clone $collection2, []],
            '(I2)/(I2+I3)'        => [clone $collection3, clone $collection4, []],
            '(I2)/(I3)'           => [clone $collection3, clone $collection5, [$item2]],

            '(I2+I3)/(I1)'        => [clone $collection4, clone $collection1, [$item2, $item3]],
            '(I2+I3)/(I1+I2)'     => [clone $collection4, clone $collection2, [$item3]],
            '(I2+I3)/(I2)'        => [clone $collection4, clone $collection3, [$item3]],
            '(I2+I3)/(I3)'        => [clone $collection4, clone $collection5, [$item2]],

            '(I3)/(I1)'           => [clone $collection5, clone $collection1, [$item3]],
            '(I3)/(I1+I2)'        => [clone $collection5, clone $collection2, [$item3]],
            '(I3)/(I2)'           => [clone $collection5, clone $collection3, [$item3]],
            '(I3)/(I2+I3)'        => [clone $collection5, clone $collection4, []],
        ];
    }

    /**
     * Checking that it can subtract items by a collection
     *
     * @param CollectionInterface $A
     * @param CollectionInterface $B
     * @param array $expectedResult
     *
     * @dataProvider subtractProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canDoSubtract(CollectionInterface $A, CollectionInterface $B, array $expectedResult)
    {
        $A->subtractItems($B);

        if (empty($expectedResult)) $this->assertEmpty($expectedResult);
        else
        {
            $expectedResultItems = [];
            $resultItems = [];

            foreach($A as $item) $resultItems[] = 'I' . $item->id();
            foreach($expectedResult as $item) $expectedResultItems[] = 'I' . $item->id();

            sort($resultItems);
            sort($expectedResultItems);

            $this->assertSame(
                implode('+', $expectedResultItems),
                implode('+', $resultItems)
            );
        }
    }

    // ----------------------------------------------------------------------------------------------------------------

    public function intersectProvider()
    {
        $this->setUp();

        $item1 = $this->createMock($this->item->getName());
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);
        /** @var EntityInterface $item1 */

        $item2 = $this->createMock($this->item->getName());
        $item2->expects($this->atLeastOnce())->method('id')->willReturn(2);
        /** @var EntityInterface $item2 */

        $item3 = $this->createMock($this->item->getName());
        $item3->expects($this->atLeastOnce())->method('id')->willReturn(3);
        /** @var EntityInterface $item3 */

        /** @var CollectionInterface $collection1 */
        $collection1 = $this->class->newInstance();
        $collection1->add($item1);

        /** @var CollectionInterface $collection2 */
        $collection2 = $this->class->newInstance();
        $collection2->add($item1);
        $collection2->add($item2);

        /** @var CollectionInterface $collection3 */
        $collection3 = $this->class->newInstance();
        $collection3->add($item2);

        /** @var CollectionInterface $collection4 */
        $collection4 = $this->class->newInstance();
        $collection4->add($item2);
        $collection4->add($item3);

        /** @var CollectionInterface $collection5 */
        $collection5 = $this->class->newInstance();
        $collection5->add($item3);

        return [
            '(I1)^(I1+I2)'        => [clone $collection1, clone $collection2, [$item1]],
            '(I1)^(I2)'           => [clone $collection1, clone $collection3, []],
            '(I1)^(I2+I3)'        => [clone $collection1, clone $collection4, []],
            '(I1)^(I3)'           => [clone $collection1, clone $collection5, []],

            '(I1+I2)^(I1)'        => [clone $collection2, clone $collection1, [$item1]],
            '(I1+I2)^(I2)'        => [clone $collection2, clone $collection3, [$item2]],
            '(I1+I2)^(I2+I3)'     => [clone $collection2, clone $collection4, [$item2]],
            '(I1+I2)^(I3)'        => [clone $collection2, clone $collection5, []],

            '(I2)^(I1)'           => [clone $collection3, clone $collection1, []],
            '(I2)^(I1+I2)'        => [clone $collection3, clone $collection2, [$item2]],
            '(I2)^(I2+I3)'        => [clone $collection3, clone $collection4, [$item2]],
            '(I2)^(I3)'           => [clone $collection3, clone $collection5, []],

            '(I2+I3)^(I1)'        => [clone $collection4, clone $collection1, []],
            '(I2+I3)^(I1+I2)'     => [clone $collection4, clone $collection2, [$item2]],
            '(I2+I3)^(I2)'        => [clone $collection4, clone $collection3, [$item2]],
            '(I2+I3)^(I3)'        => [clone $collection4, clone $collection5, [$item3]],

            '(I3)^(I1)'           => [clone $collection5, clone $collection1, []],
            '(I3)^(I1+I2)'        => [clone $collection5, clone $collection2, []],
            '(I3)^(I2)'           => [clone $collection5, clone $collection3, []],
            '(I3)^(I2+I3)'        => [clone $collection5, clone $collection4, [$item3]],
        ];
    }

    /**
     *
     * @param CollectionInterface $A
     * @param CollectionInterface $B
     * @param array $expectedResult
     *
     * @dataProvider intersectProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canDoIntersect(CollectionInterface $A, CollectionInterface $B, array $expectedResult)
    {
        $A->intersectWith($B);

        if (empty($expectedResult)) $this->assertEmpty($expectedResult);
        else
        {
            $expectedResultItems = [];
            $resultItems = [];

            foreach($A as $item) $resultItems[] = 'I' . $item->id();
            foreach($expectedResult as $item) $expectedResultItems[] = 'I' . $item->id();

            sort($resultItems);
            sort($expectedResultItems);

            $this->assertSame(
                implode('+', $expectedResultItems),
                implode('+', $resultItems)
            );
        }
    }
}